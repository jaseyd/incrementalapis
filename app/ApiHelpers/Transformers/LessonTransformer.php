<?php

namespace ApiHelpers\Transformers;

class LessonTransformer extends Transformer {
	public function Transform($lesson) {
		return [
			'title' => $lesson['title'],
			'body' => $lesson['body'],
			'active' => (boolean) $lesson['some_bool']
		];
    }
}