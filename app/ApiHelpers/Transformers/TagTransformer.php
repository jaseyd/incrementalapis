<?php

namespace ApiHelpers\Transformers;

class TagTransformer extends Transformer {
	public function Transform($tag) {
		return [
			'name' => $tag['name']
		];
    }
}