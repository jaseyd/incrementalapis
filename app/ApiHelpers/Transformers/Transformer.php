<?php

namespace ApiHelpers\Transformers;

abstract class Transformer {

	public function TransformCollection(array $items) {
    	return array_map([$this, 'transform'], $items);
    }

    public abstract function transform($item);

}