<?php

namespace App;
use App\Tag;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable = ['title','body','some_bool']; // fillable fields are allowed to be set directly.. ie, by the seeder
    // protected $hidden = ['title']; // hidden fields aren't displayed when casing to the output (JSON)

    public function tags() {
    	return $this->belongsToMany('App\Tag');
    }
}
