<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;
use App\Lesson;
use ApiHelpers\Transformers\LessonTransformer;

class LessonsController extends ApiController
{
	protected $LessonTransformer;

	function __construct(LessonTransformer $LessonTransformer) {
		$this->LessonTransformer = $LessonTransformer;
        // This doesn't work anymore
        // $this->beforeFilter('auth.basic'); //make sure to use SSL when using basic auth
        $this->middleware('auth.basic', ['only' => 'store']);
        // $this->middleware('auth.basic');
	}
    //
    public function index() {
    	// 1. Returns everything.. why would we do this? It could be massive.. need pagination
    	// 2. We have no easy way to attach meta info
    	// 3. Gives too much away about the DB. Also this rigididy between the DB and the output means we can't change the DB without affecting users of the API
    	// 4. No error checking or response codes etc. What if something went wrong? (headers/reponse codes)
    	//return Lesson::all(); // really bad practice

    	$lessons = Lesson::all();

    	return $this->respond([
    		'data' => $this->LessonTransformer->TransformCollection($lessons->all())
    	]);
    }

    public function show($id) {
    	$lesson = Lesson::find($id);

    	if(!$lesson) {
            return $this->respondNotFound('Lesson does not exist');

    	}

    	return $this->respond([
    		'data' => $this->LessonTransformer->Transform($lesson)
    	]);
    }

    public function store()
    {
        // dd(Request::input());
        // $input = $request;

        if (!Request::input('title') or !Request::input('body')) 
        {
            return $this->setStatusCode(422)->respondWithError('Parameters failed');
        }

        Lesson::create(Request::all());

        return $this->respondCreated('Lesson Created Successfully');
    }
}