<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Lesson;
use ApiHelpers\Transformers\TagTransformer;

class TagsController extends ApiController
{
	protected $tagTransformer;

	function __construct(TagTransformer $tagTransformer) {
		$this->tagTransformer = $tagTransformer;
	}

    //
    public function index($lessonid = null) {
    	
    	$tags = $this->getTags($lessonid);

    	return $this->respond([
    		'data' => $this->tagTransformer->transformCollection($tags->all())
    	]);
    }

    public function getTags($lessonid) {
    	return $lessonid ? Lesson::findOrFail($lessonid)->tags : Tag::all(); //bad
    }
}
