<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response as IlluminateResponse; // provided an alias so it doesn't clash with our Response below
use App\Lesson;
use Response;

class ApiController extends Controller {
	protected $statusCode = 200;

	public function getStatusCode() {
		return $this->statusCode;
	}

	public function setStatusCode($statusCode) {
		$this->statusCode = $statusCode;
		return $this;
	}

	public function respondNotFound($message = "Not found") {
		return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->respondWithError($message);
	}

	public function respondInternalError($message = "Internal Error") {
		return $this->setStatusCode(500)->respondWithError($message);
	}

	public function respond($data, $headers = []) {
		return Response::json($data, $this->getStatusCode(), $headers);
	}

	public function respondCreated($message = "Created successfully") {
		return $this->setStatusCode(201)->respond([
    		'message' => $message,
    		'status_code' => $this->getStatusCode()
        ]);
	}

	public function respondWithError($message) {
		return $this->respond([
        	'error' => [
        		'message' => $message,
        		'status_code' => $this->getStatusCode()
        	]
        ]);
	}
}